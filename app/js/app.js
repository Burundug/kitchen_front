//=require ../blocks/**/*.js
$(document).ready(function () {
    //burger
    if (document.querySelector(".js-burger")) {
        $(".js-burger").click(function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(".header-top").slideUp("active");
                $(".logo").removeClass("active");
                $(".phone").removeClass("active");
                $(".header-main").removeClass("active");
                $(".substrate").removeClass("active");
                $(".search-form").removeClass("active");
                $(".nav").removeClass("active");
                $(".header .container").removeClass("active");
                $(".header-info__icons").removeClass("active");
                $(".city-selectionImg").removeClass("active");
                $(".header").removeClass("active");
                enableScroll();
            } else {
                $(this).addClass("active");
                $(".header-top").slideDown("active");
                $(".logo").addClass("active");
                $(".phone").addClass("active");
                $(".header-main").addClass("active");
                $(".substrate").addClass("active");
                $(".search-form").addClass("active");
                $(".nav").addClass("active");
                $(".header .container").addClass("active");
                $(".header-info__icons").addClass("active");
                $(".city-selectionImg").addClass("active");
                $(".header").addClass("active");
                disableScroll();
            }
        });
    }
});
/*const jsBurger = document.querySelector('.js-burger'),
    headerTop = document.querySelector('.header-top');
jsBurger.addEventListener('click', () => {
    headerTop.style.display = 'flex';
})*/

/*const onCallbackModal = () => {
    const mask = document.querySelector('.mask'),
        jsModal = document.querySelector('#js-modal'),
        modalBtn = document.querySelector('.modal__btn'),
        formThanksClose = document.querySelectorAll('.formClose__btn'),
        formTthanksBtn = document.querySelectorAll('.form-thanks__btn'),
        formModal = document.querySelector('#modal-form');

    mask.style.display = 'block';
    jsModal.style.display = 'block';

    const closeWin = () => {
        closeModal();
        formModal.reset();
        formModal.classList.remove('success');
        setFilled();
    }

    modalBtn.addEventListener('click', () => {
        closeModal();
    })
    formTthanksBtn.forEach((e) => {
        e.addEventListener('click', () => {
            closeWin();
        })
    })
    formThanksClose.forEach((e) => {
        e.addEventListener('click', () => {
            closeWin();
        })
    })

    const closeModal = () => {
        mask.style.display = 'none';
        jsModal.style.display = 'none';
    }
    mask.addEventListener('click', (event) => {
        const target = event.target;

        if (target.closest('.mask')) {
            closeWin();
        }
    })
}*/
/*
document.addEventListener('DOMContentLoaded', () => {
    $('.main-slider').on('afterChange', (a, b, c) => {
        console.log($('.thumb-slider .slick-slide[data-slick-index="' + c + '"]'));
        $('.thumb-slider .slick-slide').removeClass('slick-current');
        $('.thumb-slider .slick-slide[data-slick-index="' + c + '"]').addClass('slick-current');
    })
    $('.main-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        infinite: true,
    });

    $('.thumb-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        infinite: true,
        asNavFor: '.main-slider',
        adaptiveHeight: true,
        vertical: true,
        focusOnSelect: false,
        variableHeight: true,
        variableWidth: false,
        rows: 0,
        centerMode: false,
        centerPadding: 0,
        responsive: [
            {
                breakpoint: 1056,
                settings: {
                    vertical: false,
                }
            },
            {
                breakpoint: 672,
                settings: {
                    vertical: true,
                }
            },
        ]
    });
    $('.thumb-slider .slick-slide').on('click', function (event) {
        $('.thumb-slider .slick-slide').removeClass('slick-current');
        $(this).addClass('slick-current');
        $('.main-slider').slick('slickGoTo', $(this).data('slickIndex'));
    });
});
*/
