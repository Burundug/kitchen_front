function toTop() {
    $("body, html").animate({
        scrollTop: 0
    }, 1000)
}

function topArrow() {
    var o = document.querySelector(".up")
        , c = document.querySelector(".footer");
    window.addEventListener("scroll", function() {
        var e = c.offsetTop + document.body.scrollTop
            , t = window.innerHeight;
        500 < pageYOffset && pageYOffset < e - t ? o.classList.add("active") : o.classList.remove("active")
        if (window.scrollY >= 500) {
            o.classList.add('is-visible');
        } else {
            o.classList.remove('is-visible');
        }
    })
}
topArrow();


function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function checkCookie() {
    let cookie = getCookie('confirm_cookie'),
        modal = document.getElementById('cookie');
    if (typeof cookie === "undefined") {
        modal.classList.add('active');
    }
}

function cookieConfirm() {
    let date = new Date(Date.now() + 21 * 24 * 60 * 60 * 1000),
        modal = document.getElementById('cookie');
    date = date.toUTCString();
    //document.cookie = "confirm_cookie=" + 1 + "; max-age=" + date + "; path=/";
    document.cookie = "confirm_cookie=" + 1 + "; expires=" + date + "; path=/";
    modal.classList.remove('active');
}

document.addEventListener('DOMContentLoaded', () => {
    setTimeout(function () {
        checkCookie();
    }, 10000);
})
