$("body").on("click", ".city-win", function () {
    event.preventDefault();
    $.fancybox.open({
        src  : '#js-modal',
        type : 'inline',
        opts : {
            autoFocus: false,
            touch: false,
            /*'smallBtn': true,*/
            toolbar: false,
            hideScrollbar: false,
        }
    });
});