function intoSlider() {
    let slider = document.querySelector('.recommend');
    window.slider = false;

    function init(slider) {
        function pagin(slick, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            pagin.dataset.width = 100 / total;
            pagin.style.width = 100 / total + '%';
        }

        function changePagin(slick, current, next, direction, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            next = next + 1;
            let paginW = 100 / total * next;
            pagin.dataset.width = paginW;
            pagin.style.width = paginW + '%';
        }

        if (slider && !window.slider) {
            window.slider = true;
            $(slider).on('init', function (event, slick, currentSlide, nextSlide) {
                pagin(slick, 3);
            })
            $(slider).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                if (nextSlide > currentSlide) {
                    changePagin(slick, currentSlide, nextSlide, true, 3);
                } else {
                    changePagin(slick, currentSlide, nextSlide, false, 3);
                }
            });
            $(slider).slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                prevArrow: $('.recommend-prev'),
                nextArrow: $('.recommend-next'),
                //variableWidth: true,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            //variableWidth: false,
                        }
                    }
                ]
            });
        }
    }
    if (slider) {
        document.addEventListener('DOMContentLoaded', function () {
            init(slider);
            /*if (window.innerWidth < 990) {
                init(slider);
            }*/
        });
      /*  window.addEventListener('resize', function () {
            if (window.innerWidth < 990) {
                init(slider);
            } else {
                window.slider = false;
                if (slider.classList.contains('slick-initialized')) {
                    $(slider).slick('unslick');
                }
            }
        });*/
    }
}

intoSlider();
