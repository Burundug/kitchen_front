$(document).ready(function () {
    $('.tel').inputmask("+7 (999)-999-99-99", {"placeholder": "_"});

    const anchors = document.querySelectorAll('a.scroll-link')

    Array.prototype.forEach.call(anchors, function (anchor) {
        anchor.addEventListener('click', function (e) {
            e.preventDefault()
            const blockID = anchor.getAttribute('href').substr(1)
            $(".js-nav").removeClass("active");
            $(".js-burger").removeClass("active");
            $("body").removeClass("overflow");
            document.getElementById(blockID).scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            })
        })
    });
});

const populatedFalse = () => {
    const input = document.querySelector('#main-form #name');
    const input2 = document.querySelector('#main-form #tel');
    const input3 = document.querySelector('#main-form #email');

    input.classList.remove('populated');
    input2.classList.remove('populated');
    input3.classList.remove('populated');
}

// рекапча
function onCallbackSubmit(token) {
    const form = document.querySelector('#main-form');


    $(form).parsley().on('form:success', function () {
        form.classList.add('success');
        setTimeout(() => {
            form.reset();
            setFilled();
            populatedFalse();
            form.classList.remove('success');
        }, 4000);
    });

    $(form).parsley().validate(); // всегда внизу
}
function onCallbackSubmitRew(token) {
    const form = document.querySelector('#reviews-form');

    $(form).parsley().on('form:success', function () {
        form.classList.add('success');
        setTimeout(() => {
            form.reset();
            setFilled();
            form.classList.remove('success');
        }, 4000);
    });

    $(form).parsley().validate(); // всегда внизу
}

function callbackClose() {
    const form = document.querySelector('#main-form');
    form.reset();
    setFilled();
    form.classList.remove('success');
}
function callbackCloseRew() {
    const form = document.querySelector('#reviews-form');
    form.reset();
    setFilled();
    form.classList.remove('success');
}

const closeFancy = () => {
    $.fancybox.close();
}
/*
function doMenuSearch(input) {
    let search = document.querySelector('.menu-detail__wrap-search').clientHeight,
        wrap = document.querySelector('.menu-detail__wrap'),
        results = document.querySelector('.search-results'),
        searchClose = document.querySelector(".search-holder__close");
    if(input.value.length) {
        results.style.height = wrap.clientHeight - parseInt(getComputedStyle(wrap,null).getPropertyValue('padding-Top')) - parseInt(getComputedStyle(wrap,null).getPropertyValue('padding-Bottom')) - search + 'px';
        results.style.display = 'block';
    }
    searchClose.addEventListener('click', () => {
        input.value = '';
        results.style.display = 'none';
    });
}*/
// для поднятия label
function setFilled() {
    let text = document.querySelectorAll('.iText');
    Array.prototype.forEach.call(text, function (el) {
        if (el.classList.contains('pmask')) {
            el.parentNode.classList.add('filled');
        }

        if (el.value.length > 0) {
            el.parentNode.classList.add('filled');
            el.parentNode.classList.add('lbhidd');
        } else {
            el.parentNode.classList.remove('filled');
        }
        el.addEventListener('focus', () => {
            el.parentNode.classList.add('filled');
            el.parentNode.classList.remove('lbhidd');
        });
        el.addEventListener('blur', () => {
            if (el.value.length === 0) {
                el.parentNode.classList.remove('filled');
            } else {
                el.parentNode.classList.add('lbhidd');
            }
        })
    });
}
setFilled();


/*const btn = document.querySelectorAll('.btn');
if (btn) {
    btn.forEach((e) => {
        e.addEventListener('click', (btn) => {
            e.classList.add('active');
            setTimeout(() => {
                e.classList.remove('active');
            }, 100);
        })
    })
}*/
$(document).ready(function () {
    window.Parsley.on('field:error', function (e) {
        this.$element[0].parentNode.classList.add('error');
    });
    window.Parsley.on('field:success', function (e) {
        this.$element[0].parentNode.classList.remove('error');
    });
});
$(function() {
    $('input').on('change', function() {
        var input = $(this);
        if (input.val().length) {
            input.addClass('populated');
        } else {
            input.removeClass('populated');
        }
    });
    $('#form_textarea').on('change', function() {
        var input = $(this);
        if (input.val().length) {
            input.addClass('populated');
        } else {
            input.removeClass('populated');
        }
    });

   /* setTimeout(function() {
        $('#name').trigger('focus');
    }, 500);*/
});