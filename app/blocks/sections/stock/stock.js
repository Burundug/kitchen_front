$('.stock-slider').slick({
    infinite: true,
    slidesToShow: 1,
    dots: true,
    arrows: false,
    dotsClass: 'main2-dots',
    autoplay: true
});

const autoHei = () => {
    window.addEventListener('resize', function () {
        if (window.innerWidth > 767) {
            $('.stock').each(function (){
                let thc = $(this),
                    thcHeight = thc.find('.card').outerHeight();
                thc.find('.bg-i').css('max-height', thcHeight);
            });
        }
    });
}
autoHei();