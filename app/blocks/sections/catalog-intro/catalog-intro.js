$('.js-catalogSlider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 2,
    //variableWidth: true,
    prevArrow: $('.catIn-prev'),
    nextArrow: $('.catIn-next'),
    responsive: [
        {
            breakpoint: 2400,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                variableWidth: true,
                //variableWidth: false,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                //variableWidth: false,
            }
        }
    ]
});
/*
function catalogSlider() {
    let slider = document.querySelector('.catalog-intro');
    window.slider = false;

    function init(slider) {
        function paginCat(slick, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            pagin.dataset.width = 100 / total;
            pagin.style.width = 100 / total + '%';
        }

        function changePaginCat(slick, current, next, direction, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            next = next + 1;
            let paginW = 100 / total * next;
            pagin.dataset.width = paginW;
            pagin.style.width = paginW + '%';
        }

        if (slider && !window.slider) {
            window.slider = true;
            $(slider).on('init', function (event, slick, currentSlide, nextSlide) {
                paginCat(slick, 8);
            })
            $(slider).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                if (nextSlide > currentSlide) {
                    changePaginCat(slick, currentSlide, nextSlide, true, 8);
                } else {
                    changePaginCat(slick, currentSlide, nextSlide, false, 8);
                }
            });
            $(slider).slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 2,
                variableWidth: true,
                prevArrow: $('.catIn-prev'),
                nextArrow: $('.catIn-next'),
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            //variableWidth: false,
                        }
                    }
                ]
            });
        }
    }
    if (slider) {
        document.addEventListener('DOMContentLoaded', function () {
            init(slider);
            /!*if (window.innerWidth < 990) {
                init(slider);
            }*!/
        });
        /!*  window.addEventListener('resize', function () {
              if (window.innerWidth < 990) {
                  init(slider);
              } else {
                  window.slider = false;
                  if (slider.classList.contains('slick-initialized')) {
                      $(slider).slick('unslick');
                  }
              }
          });*!/
    }
}

catalogSlider();
*/
