const autoHeiArt = () => {
    window.addEventListener('resize', function () {
        if (window.innerWidth > 767) {
            $('.article').each(function (){
                let thc = $(this),
                    thcHeight = thc.find('.card').outerHeight();
                thc.find('.bg-i').css('max-height', thcHeight);
            });
        }
    });
}
autoHeiArt();