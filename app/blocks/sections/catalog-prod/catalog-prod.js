$(function () {
    $(".accordionChoice__item .accordionChoice__item-title").on("click", function (e) {
        e.preventDefault();
        var $this = $(this);

        if (!$this.hasClass("active")) {
            $(".accordionChoice__content").slideUp(400);
            $(".accordionChoice__item .accordionChoice__item-title").removeClass("active");
        }
        $this.toggleClass("active");
        $this.next().slideToggle();
    });
});

//$('#custom-select').ddslick({});

$(document).ready(function () {
    $(function () {
        $(".accordionInner__item .accordionInner__title").on("click", function (e) {
            e.preventDefault();
            var $this = $(this);

            if (!$this.hasClass("active")) {
                $(".accordionInner__content").slideUp(400);
                $(".accordionInner__item .accordionInner__title").removeClass("active");
            }
            $this.toggleClass("active");
            $this.next().slideToggle();
        });
    });
});

/*Dropdown Menu*/
const selectItem = () => {
    const substrate = document.querySelector('.substrate');
    $('.dropdown').click(function (e) {
        if (e.target === $(this)[0] && !e.target.classList.contains('active') ) {
            $(this).attr('tabindex', 1).focus();
            $(this).toggleClass('active');
            $(this).find('.dropdown-menu').slideToggle(300);
            if(window.innerWidth < 450) {
                substrate.classList.add('active');
            }
        }
    });

    $('.dropdown').on('change', function (){
        $(this).find('.dropdown-menu').slideUp(300);
        $(this).removeClass('active');
        if(window.innerWidth < 450) {
            substrate.classList.remove('active');
        }
    });

    $('.dropdown').focusout(function () {
        $(this).removeClass('active');
        $(this).find('.dropdown-menu').slideUp(300);
    });
    $('.dropdown .dropdown-menu li').click(function () {
        $(this).parents('.dropdown').find('span').text($(this).text());
    });
}
selectItem();

/*End Dropdown Menu*/

const catalogItems = document.querySelector('.catalogItems'),
    line = document.querySelector('.line'),
    lineCard = document.querySelector('.lineCard'),
    ch = document.querySelector('.ch');

    if(line) {
        line.addEventListener('click', () => {
            catalogItems.classList.add('lineCard');
            line.classList.add('active');
            ch.classList.remove('active');
        })
    }
    if(ch) {
        ch.addEventListener('click', () => {
            catalogItems.classList.remove('lineCard');
            line.classList.remove('active');
            ch.classList.add('active');
        })
    }

const filterProdBtn = document.querySelector(".filter-prod__btn"),
    filterProd = document.querySelector('.filter-prod'),
    filterProdResult = document.querySelector('.filter-prod__result'),
    filterProdCloseBtn = document.querySelector('.filter-prod__close-btn'),
    substrateBg = document.querySelector('.substrate-bg'),
    header = document.querySelector('.header');

    if(filterProdBtn) {
        filterProdBtn.addEventListener('click', () => {
            filterProd.classList.add('active');
            filterProdResult.style.display = 'none';
            substrateBg.classList.add('active');
            header.style.zIndex = '800';
            disableScroll();
        })
    }
    if(filterProdCloseBtn) {
        filterProdCloseBtn.addEventListener('click', () => {
            filterProd.classList.remove('active');
            filterProdResult.style.display = 'block';
            substrateBg.classList.remove('active');
            header.style.zIndex = '902';
            enableScroll();
        })
    }



