/*
function newSlider() {
    let slider = document.querySelector('.newItem-slider');
    window.slider = false;

    function init(slider) {
        function paginNew(slick, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            pagin.dataset.width = 100 / total;
            pagin.style.width = 100 / total + '%';
        }

        function changePaginNew(slick, current, next, direction, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            next = next + 1;
            let paginW = 100 / total * next;
            pagin.dataset.width = paginW;
            pagin.style.width = paginW + '%';
        }

        if (slider && !window.slider) {
            window.slider = true;
            $(slider).on('init', function (event, slick, currentSlide, nextSlide) {
                paginNew(slick, 6);
            })
            $(slider).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                if (nextSlide > currentSlide) {
                    changePaginNew(slick, currentSlide, nextSlide, true, 6);
                } else {
                    changePaginNew(slick, currentSlide, nextSlide, false, 6);
                }
            });
            $(slider).slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                //prevArrow: $('.recommend-prev'),
                //nextArrow: $('.recommend-next'),
                //variableWidth: true,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            //variableWidth: false,
                        }
                    }
                ]
            });
        }
    }
    if (slider) {
        document.addEventListener('DOMContentLoaded', function () {
            init(slider);
            if (window.innerWidth < 768) {
                init(slider);
            }
        });
        window.addEventListener('resize', function () {
            if (window.innerWidth < 768) {
                init(slider);
            } else {
                window.slider = false;
                if (slider.classList.contains('slick-initialized')) {
                    $(slider).slick('unslick');
                }
            }
        });
    }
}

newSlider();
*/
