function initMainSlider() {
    let slider = document.querySelector('.intro-wrap');
    if(slider) {
        $(slider).on('beforeChange', function (event, slick, currentSlide, nextSlide){
            let current =  slick.$slides.get(currentSlide),
                next = slick.$slides.get(nextSlide);
            console.log(current);
            if (currentSlide < nextSlide) {
                current.classList.add('next');
            } else {
                console.log('0');
            }
        });
        $(slider).on('afterChange', function (event, slick, currentSlide, nextSlide){

            let current =  slick.$slides.get(currentSlide),
                prev = currentSlide > 0 ? slick.$slides.get(currentSlide - 1) : slick.$slides.get(0),
                next = slick.$slides.get(nextSlide);
            prev.classList.remove('next');
        });
        $(slider).slick({
            infinite: true,
            slidesToShow: 1,
            dots: true,
            speed: 500,
            rows: 0,
            verticalSwiping: true,
            //variableWidth: true,
            prevArrow: $('.intro-prev'),
            nextArrow: $('.intro-next'),
            dotsClass: 'main-dots',
            vertical: true
        });
    }
}
document.addEventListener('DOMContentLoaded', function () {
    initMainSlider();
})