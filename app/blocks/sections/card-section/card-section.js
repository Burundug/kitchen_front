function tabChangeCard(btn) {
    let data = btn.dataset.tab,
        sections = document.querySelectorAll('.card-tabs__wrap'),
        tabs = document.querySelectorAll('.card-descr__wrap a')

    sections.forEach(el => {
        if (el.dataset.tab === data) {
            el.classList.remove('d-n');
        } else {
            el.classList.add('d-n')
        }
    })

    tabs.forEach(el => {
        if (el.dataset.tab === data) {
            el.classList.add('active');
        } else {
            el.classList.remove('active')
        }
    })
}
function toggleTabCard(btn) {
    let tabs = document.querySelectorAll('.card-descr');
    tabs.forEach(el => {
        if (btn.parentNode.parentNode !== el) {
            el.classList.remove('active');
        } else {
            btn.parentNode.parentNode.classList.toggle('active')
        }
    })
}

const reviewsForm = document.querySelector('.reviews-form'),
    reviewsTabBtn = document.querySelector('.reviews-tab__btn');
    if(reviewsTabBtn) {
        reviewsTabBtn.addEventListener('click', () => {
            reviewsForm.classList.add('active');
        })
    }
$(window).scroll(function () {
    let s = document.querySelector(".card-Topsection"),
        b = document.querySelector(".breadcrumbs"),
        p = document.querySelector(".header-main"),
        cs = document.querySelector('.card-section');
    if(s, b, p, cs) {
        let sumHeigtTillCard = b.offsetHeight;
        let treck = cs.offsetHeight - p.offsetHeight - b.offsetHeight - 150;

        if(sumHeigtTillCard) {
            if($(this).scrollTop() > sumHeigtTillCard) {
                if(".card-sm") {
                    $(".card-sm").addClass("fixed");
                }
            } else {
                if(".card-sm") {
                    $(".card-sm").removeClass("fixed");
                }
            }
        }
        if(treck) {
            if($(this).scrollTop() > treck) {
                $(".card-sm").removeClass("fixed");
            }
        }
    }
});