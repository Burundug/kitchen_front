if($(window).width() <= 1201) {
    var action;
    action = 'click';
} else {
    action = 'mouseover';
}
$(document).on(action, '.chrait svg', function(e) {
    console.log('ok');
    var rating = $(this).data('rating');
    var parent = $(this).parent();
    $(parent).children('svg[data-rating]').removeClass('filled');
    for (var i = 1; i < rating; i++) {
        $(parent).children('svg[data-rating=' + i + ']').addClass('filled');
    }
    $(this).addClass('filled');
});
function raitThis(svg) {
    var data = $(svg).data('rating');
    $(svg).parent().siblings('input').attr('value', data);
    if ($(window).width() < 1201) {
        var parent = $(svg).parent();
        $(parent).children('svg[data-rating]').removeClass('filled');
        for (var i = 1; i < data; i++) {
            $(parent).children('svg[data-rating=' + i + ']').addClass('filled');
        }
        $(svg).addClass('filled');
    }
}
$(".small").mouseenter(function (e) {
    var t;
    1023 < $(window).width() && !$(this).hasClass("column") && (t = $(this).height(), $(this).css({height: t}), $(this).children().css({position: "absolute"}), $(this).addClass("active"))
}); $(".small").mouseleave(function (e) {
    1023 < $(window).width() && !$(this).hasClass("column") && ($(this).css({height: "auto"}), $(this).children().css({position: "relative"}), $(this).removeClass("active"))
});
