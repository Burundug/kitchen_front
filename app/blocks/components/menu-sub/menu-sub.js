$(document).ready(function () {
    $(function () {
        if (window.innerWidth < 768) {
            $(".accordion__item .accordion__title").on("click", function (e) {
                e.preventDefault();
                var $this = $(this);

                if (!$this.hasClass("active")) {
                    $(".accordion__content").slideUp(400);
                    $(".accordion__item .accordion__title").removeClass("active");
                }
                $this.toggleClass("active");
                $this.next().slideToggle();
            });
        }
    });
});