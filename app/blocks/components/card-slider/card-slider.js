$('.slider-intro').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-controll',
    //dots: false,
    infinite: true,
    focusOnSelect: true,
    fade: true,
    arrows: false,
    //cssEase: 'linear',
    //adaptiveHeight: true,
    dots: true,
    speed: 300,
    //accessibility: false,
    appendDots: '.card-content-dots',
    customPaging: function(slider, i) {
        var colors = ['Синий', 'Зеленый', 'Голубой', 'Розовый', 'Красный', 'Черный', 'Оранжевый', 'Белый', 'Бордовый', 'Малиновый', 'Фиолетовый', 'Пурпурный', 'Сиреневый', 'Салатовый', 'Серый'];
        return '<button class="tab" data-color="'+colors[i]+'"></button>';
    },

});

$('.slider-controll').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    //vertical: false,
    dots: false,
    infinite: true,
    asNavFor: '.slider-intro',
    focusOnSelect: true,
    centerMode: false,
    prevArrow: $('.card-prev'),
    nextArrow: $('.card-next'),
    vertical: true,
    //variableWidth: true
    responsive: [
        {
            breakpoint: 1348,
            settings: {
                vertical: false,
            }
        },
     /*   {
            breakpoint: 991,
            settings: {
                vertical: true,
            }
        },
        {
            breakpoint: 481,
            settings: {
                vertical: false,

            }
        }*/
    ]
});
$(document).on('click', '.card-content-dots button', function(){
    var color = $(this).data('color');
    $('[name=color]').val(color);
})

$('[name=color]').val( $('.card-content-dots .slick-active button').data('color') );
