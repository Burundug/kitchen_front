$(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
        $(".header").addClass("fixed");
    } else {
        $(".header").removeClass("fixed");
    }
});
$(function () {
    $("ul.js-tab-btn-menu").on("click", "li:not(.active)", function () {
        $(this)
            .addClass("active")
            .siblings()
            .removeClass("active")
            .closest("div.js-tabs-menu")
            .find("div.js-tab-content-menu")
            .removeClass("active")
            .eq($(this).index())
            .addClass("active");
    });
 });

const headerMenuBtn = document.querySelector('.header-menu > button'),
    menuDetail = document.querySelector('.menu-detail'),
    closeBtn = document.querySelector('.closeBtnMenu'),
    userLkPerson = document.querySelector('.user-lk__person'),
    mainIcon = document.querySelector('.footer-menu__icon.main-icon'),
    substrate = document.querySelector('.substrate'),
    headSub = document.querySelector('.head-sub'),
    tabsMenuContent = document.querySelector('.tabs-menu__content');

    if(headerMenuBtn) {
        headerMenuBtn.addEventListener('click', () => {
            menuDetail.classList.add('active');
            substrate.style.display = "block";
        });
    }
    if(mainIcon) {
        mainIcon.addEventListener('click', () => {
            menuDetail.classList.add('active');
            substrate.style.display = "block";
        })
    }
    if(closeBtn) {
        closeBtn.addEventListener('click', () => {
            menuDetail.classList.remove('active');
            substrate.style.display = "none";
        });
    }
   /* if(headSub) {
        headSub.addEventListener('click', () => {
            tabsMenuContent.classList.remove('active');
        })
    }*/
    if(substrate) {
        substrate.addEventListener('click', () => {
            menuDetail.classList.remove('active');
            substrate.style.display = "none";
        });
    }
    if(userLkPerson) {
        userLkPerson.addEventListener('mouseenter', () => {
            substrate.style.display = 'block';
        })
        userLkPerson.addEventListener('mouseleave', () => {
            substrate.style.display = 'none';
        })
    }

   /* const  closeSub = (btn) => {
        //this.closest(tabsMenuContent).classList.remove('active');
        tabsMenuContent.classList.remove('active');
    }*/


const disableScroll = () => {
    const widthScroll = window.innerWidth - document.body.offsetWidth;
    document.body.dbScrollY = window.scrollY;
    document.body.style.cssText = `
        position: fixed;
        top: ${-window.scrollY}px;
        left: 0;
        width: 100%;
        height: 100vh;
        overflow: hidden;
        padding-right: ${widthScroll}px;
    `;
};
const enableScroll = () => {
    document.body.style.cssText = '';
    window.scroll({
        top: document.body.dbScrollY,
    })
};
// многоуровневое мобильное меню
function siblingsEl(array, need) {
    var b = [];
    for (var i = array.length; i--;) {
        if (array[i].classList.contains(need)) {
            b.push(array[i]);
        }
    }
    return b;
}
function toggleSub(btn) {
    let li = siblingsEl(Array.from(btn.parentNode.parentNode.children),'hasSub');
   /* if(window.innerWidth < 650) {*/
        let hasSub = li;
        hasSub.forEach(el => {
            if (btn.parentNode !== el) {
                el.classList.remove('active');
            } else {
                el.classList.toggle('active');
            }
        });
    /*}*/
}
function closeSub(btn) {
    let parent = getClosest(btn,'hasSub');
    parent.classList.remove('active');
}
function getClosest(el, tag) {
    do {
        if (el.classList.contains(tag)) {
            return el;
        }
    } while (el = el.parentNode);

    return null;
}
// конец многоуровневого мобильного меню
if(window.innerWidth > 449 ) {
    document.querySelector("body > div > header > div.header-main > div > div.header-menu > div > div > ul > li:nth-child(1)").classList.add('active');

}
