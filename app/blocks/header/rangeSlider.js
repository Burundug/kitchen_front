const customRangeSlider = () => {
    const range = document.querySelector('.range_slider'),
        inputs = document.querySelectorAll('.range--input');

    if(range) {
        noUiSlider.create(range, {
            start: [0, 58000],
            connect: true,
            margin: 100,
            range: {
                'min': 0,
                'max': 58000
            }
        });
        range.noUiSlider.on('update', function (values, handle) {
            let value1 =  Math.round(values[0]).toLocaleString('ru'),
                value2 = Math.round(values[1]).toLocaleString('ru');
            inputs[0].value = 'от ' + value1;
            inputs[1].value = 'до ' + value2;
        });
    }
    inputs.forEach(e => {
        e.addEventListener('focus', () => {
            e.value = '';
            //e.style.border = '1px solid green'
        })
    })
}
customRangeSlider();
