"use strict";

function toggleCrumbs(e) {
  "A" !== event.target.nodeName && e.classList.add("open");
}

$('.slider-intro').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.slider-controll',
  //dots: false,
  infinite: true,
  focusOnSelect: true,
  fade: true,
  arrows: false,
  //cssEase: 'linear',
  //adaptiveHeight: true,
  dots: true,
  speed: 300,
  //accessibility: false,
  appendDots: '.card-content-dots',
  customPaging: function customPaging(slider, i) {
    var colors = ['Синий', 'Зеленый', 'Голубой', 'Розовый', 'Красный', 'Черный', 'Оранжевый', 'Белый', 'Бордовый', 'Малиновый', 'Фиолетовый', 'Пурпурный', 'Сиреневый', 'Салатовый', 'Серый'];
    return '<button class="tab" data-color="' + colors[i] + '"></button>';
  }
});
$('.slider-controll').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  //vertical: false,
  dots: false,
  infinite: true,
  asNavFor: '.slider-intro',
  focusOnSelect: true,
  centerMode: false,
  prevArrow: $('.card-prev'),
  nextArrow: $('.card-next'),
  vertical: true,
  //variableWidth: true
  responsive: [{
    breakpoint: 1348,
    settings: {
      vertical: false
    }
  }
  /*   {
         breakpoint: 991,
         settings: {
             vertical: true,
         }
     },
     {
         breakpoint: 481,
         settings: {
             vertical: false,
          }
     }*/
  ]
});
$(document).on('click', '.card-content-dots button', function () {
  var color = $(this).data('color');
  $('[name=color]').val(color);
});
$('[name=color]').val($('.card-content-dots .slick-active button').data('color'));

if ($(window).width() <= 1201) {
  var action;
  action = 'click';
} else {
  action = 'mouseover';
}

$(document).on(action, '.chrait svg', function (e) {
  console.log('ok');
  var rating = $(this).data('rating');
  var parent = $(this).parent();
  $(parent).children('svg[data-rating]').removeClass('filled');

  for (var i = 1; i < rating; i++) {
    $(parent).children('svg[data-rating=' + i + ']').addClass('filled');
  }

  $(this).addClass('filled');
});

function raitThis(svg) {
  var data = $(svg).data('rating');
  $(svg).parent().siblings('input').attr('value', data);

  if ($(window).width() < 1201) {
    var parent = $(svg).parent();
    $(parent).children('svg[data-rating]').removeClass('filled');

    for (var i = 1; i < data; i++) {
      $(parent).children('svg[data-rating=' + i + ']').addClass('filled');
    }

    $(svg).addClass('filled');
  }
}

$(".small").mouseenter(function (e) {
  var t;
  1023 < $(window).width() && !$(this).hasClass("column") && (t = $(this).height(), $(this).css({
    height: t
  }), $(this).children().css({
    position: "absolute"
  }), $(this).addClass("active"));
});
$(".small").mouseleave(function (e) {
  1023 < $(window).width() && !$(this).hasClass("column") && ($(this).css({
    height: "auto"
  }), $(this).children().css({
    position: "relative"
  }), $(this).removeClass("active"));
});
$(document).ready(function () {
  $(function () {
    if (window.innerWidth < 768) {
      $(".accordion__item .accordion__title").on("click", function (e) {
        e.preventDefault();
        var $this = $(this);

        if (!$this.hasClass("active")) {
          $(".accordion__content").slideUp(400);
          $(".accordion__item .accordion__title").removeClass("active");
        }

        $this.toggleClass("active");
        $this.next().slideToggle();
      });
    }
  });
});

function toTop() {
  $("body, html").animate({
    scrollTop: 0
  }, 1000);
}

function topArrow() {
  var o = document.querySelector(".up"),
      c = document.querySelector(".footer");
  window.addEventListener("scroll", function () {
    var e = c.offsetTop + document.body.scrollTop,
        t = window.innerHeight;
    500 < pageYOffset && pageYOffset < e - t ? o.classList.add("active") : o.classList.remove("active");

    if (window.scrollY >= 500) {
      o.classList.add('is-visible');
    } else {
      o.classList.remove('is-visible');
    }
  });
}

topArrow();

function getCookie(name) {
  var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function checkCookie() {
  var cookie = getCookie('confirm_cookie'),
      modal = document.getElementById('cookie');

  if (typeof cookie === "undefined") {
    modal.classList.add('active');
  }
}

function cookieConfirm() {
  var date = new Date(Date.now() + 21 * 24 * 60 * 60 * 1000),
      modal = document.getElementById('cookie');
  date = date.toUTCString(); //document.cookie = "confirm_cookie=" + 1 + "; max-age=" + date + "; path=/";

  document.cookie = "confirm_cookie=" + 1 + "; expires=" + date + "; path=/";
  modal.classList.remove('active');
}

document.addEventListener('DOMContentLoaded', function () {
  setTimeout(function () {
    checkCookie();
  }, 10000);
});
$(window).scroll(function () {
  if ($(this).scrollTop() > 0) {
    $(".header").addClass("fixed");
  } else {
    $(".header").removeClass("fixed");
  }
});
$(function () {
  $("ul.js-tab-btn-menu").on("click", "li:not(.active)", function () {
    $(this).addClass("active").siblings().removeClass("active").closest("div.js-tabs-menu").find("div.js-tab-content-menu").removeClass("active").eq($(this).index()).addClass("active");
  });
});
var headerMenuBtn = document.querySelector('.header-menu > button'),
    menuDetail = document.querySelector('.menu-detail'),
    closeBtn = document.querySelector('.closeBtnMenu'),
    userLkPerson = document.querySelector('.user-lk__person'),
    mainIcon = document.querySelector('.footer-menu__icon.main-icon'),
    substrate = document.querySelector('.substrate'),
    headSub = document.querySelector('.head-sub'),
    tabsMenuContent = document.querySelector('.tabs-menu__content');

if (headerMenuBtn) {
  headerMenuBtn.addEventListener('click', function () {
    menuDetail.classList.add('active');
    substrate.style.display = "block";
  });
}

if (mainIcon) {
  mainIcon.addEventListener('click', function () {
    menuDetail.classList.add('active');
    substrate.style.display = "block";
  });
}

if (closeBtn) {
  closeBtn.addEventListener('click', function () {
    menuDetail.classList.remove('active');
    substrate.style.display = "none";
  });
}
/* if(headSub) {
     headSub.addEventListener('click', () => {
         tabsMenuContent.classList.remove('active');
     })
 }*/


if (substrate) {
  substrate.addEventListener('click', function () {
    menuDetail.classList.remove('active');
    substrate.style.display = "none";
  });
}

if (userLkPerson) {
  userLkPerson.addEventListener('mouseenter', function () {
    substrate.style.display = 'block';
  });
  userLkPerson.addEventListener('mouseleave', function () {
    substrate.style.display = 'none';
  });
}
/* const  closeSub = (btn) => {
     //this.closest(tabsMenuContent).classList.remove('active');
     tabsMenuContent.classList.remove('active');
 }*/


var disableScroll = function disableScroll() {
  var widthScroll = window.innerWidth - document.body.offsetWidth;
  document.body.dbScrollY = window.scrollY;
  document.body.style.cssText = "\n        position: fixed;\n        top: ".concat(-window.scrollY, "px;\n        left: 0;\n        width: 100%;\n        height: 100vh;\n        overflow: hidden;\n        padding-right: ").concat(widthScroll, "px;\n    ");
};

var enableScroll = function enableScroll() {
  document.body.style.cssText = '';
  window.scroll({
    top: document.body.dbScrollY
  });
}; // многоуровневое мобильное меню


function siblingsEl(array, need) {
  var b = [];

  for (var i = array.length; i--;) {
    if (array[i].classList.contains(need)) {
      b.push(array[i]);
    }
  }

  return b;
}

function toggleSub(btn) {
  var li = siblingsEl(Array.from(btn.parentNode.parentNode.children), 'hasSub');
  /* if(window.innerWidth < 650) {*/

  var hasSub = li;
  hasSub.forEach(function (el) {
    if (btn.parentNode !== el) {
      el.classList.remove('active');
    } else {
      el.classList.toggle('active');
    }
  });
  /*}*/
}

function closeSub(btn) {
  var parent = getClosest(btn, 'hasSub');
  parent.classList.remove('active');
}

function getClosest(el, tag) {
  do {
    if (el.classList.contains(tag)) {
      return el;
    }
  } while (el = el.parentNode);

  return null;
} // конец многоуровневого мобильного меню


if (window.innerWidth > 449) {
  document.querySelector("body > div > header > div.header-main > div > div.header-menu > div > div > ul > li:nth-child(1)").classList.add('active');
}

var customRangeSlider = function customRangeSlider() {
  var range = document.querySelector('.range_slider'),
      inputs = document.querySelectorAll('.range--input');

  if (range) {
    noUiSlider.create(range, {
      start: [0, 58000],
      connect: true,
      margin: 100,
      range: {
        'min': 0,
        'max': 58000
      }
    });
    range.noUiSlider.on('update', function (values, handle) {
      var value1 = Math.round(values[0]).toLocaleString('ru'),
          value2 = Math.round(values[1]).toLocaleString('ru');
      inputs[0].value = 'от ' + value1;
      inputs[1].value = 'до ' + value2;
    });
  }

  inputs.forEach(function (e) {
    e.addEventListener('focus', function () {
      e.value = ''; //e.style.border = '1px solid green'
    });
  });
};

customRangeSlider();
$("body").on("click", ".city-win", function () {
  event.preventDefault();
  $.fancybox.open({
    src: '#js-modal',
    type: 'inline',
    opts: {
      autoFocus: false,
      touch: false,

      /*'smallBtn': true,*/
      toolbar: false,
      hideScrollbar: false
    }
  });
});

var autoHeiArt = function autoHeiArt() {
  window.addEventListener('resize', function () {
    if (window.innerWidth > 767) {
      $('.article').each(function () {
        var thc = $(this),
            thcHeight = thc.find('.card').outerHeight();
        thc.find('.bg-i').css('max-height', thcHeight);
      });
    }
  });
};

autoHeiArt();

function tabChangeCard(btn) {
  var data = btn.dataset.tab,
      sections = document.querySelectorAll('.card-tabs__wrap'),
      tabs = document.querySelectorAll('.card-descr__wrap a');
  sections.forEach(function (el) {
    if (el.dataset.tab === data) {
      el.classList.remove('d-n');
    } else {
      el.classList.add('d-n');
    }
  });
  tabs.forEach(function (el) {
    if (el.dataset.tab === data) {
      el.classList.add('active');
    } else {
      el.classList.remove('active');
    }
  });
}

function toggleTabCard(btn) {
  var tabs = document.querySelectorAll('.card-descr');
  tabs.forEach(function (el) {
    if (btn.parentNode.parentNode !== el) {
      el.classList.remove('active');
    } else {
      btn.parentNode.parentNode.classList.toggle('active');
    }
  });
}

var reviewsForm = document.querySelector('.reviews-form'),
    reviewsTabBtn = document.querySelector('.reviews-tab__btn');

if (reviewsTabBtn) {
  reviewsTabBtn.addEventListener('click', function () {
    reviewsForm.classList.add('active');
  });
}

$(window).scroll(function () {
  var s = document.querySelector(".card-Topsection"),
      b = document.querySelector(".breadcrumbs"),
      p = document.querySelector(".header-main"),
      cs = document.querySelector('.card-section');

  if (s, b, p, cs) {
    var sumHeigtTillCard = b.offsetHeight;
    var treck = cs.offsetHeight - p.offsetHeight - b.offsetHeight - 150;

    if (sumHeigtTillCard) {
      if ($(this).scrollTop() > sumHeigtTillCard) {
        if (".card-sm") {
          $(".card-sm").addClass("fixed");
        }
      } else {
        if (".card-sm") {
          $(".card-sm").removeClass("fixed");
        }
      }
    }

    if (treck) {
      if ($(this).scrollTop() > treck) {
        $(".card-sm").removeClass("fixed");
      }
    }
  }
});
$('.js-catalogSlider').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 2,
  //variableWidth: true,
  prevArrow: $('.catIn-prev'),
  nextArrow: $('.catIn-next'),
  responsive: [{
    breakpoint: 2400,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      variableWidth: true //variableWidth: false,

    }
  }, {
    breakpoint: 480,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1 //variableWidth: false,

    }
  }]
});
/*
function catalogSlider() {
    let slider = document.querySelector('.catalog-intro');
    window.slider = false;

    function init(slider) {
        function paginCat(slick, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            pagin.dataset.width = 100 / total;
            pagin.style.width = 100 / total + '%';
        }

        function changePaginCat(slick, current, next, direction, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            next = next + 1;
            let paginW = 100 / total * next;
            pagin.dataset.width = paginW;
            pagin.style.width = paginW + '%';
        }

        if (slider && !window.slider) {
            window.slider = true;
            $(slider).on('init', function (event, slick, currentSlide, nextSlide) {
                paginCat(slick, 8);
            })
            $(slider).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                if (nextSlide > currentSlide) {
                    changePaginCat(slick, currentSlide, nextSlide, true, 8);
                } else {
                    changePaginCat(slick, currentSlide, nextSlide, false, 8);
                }
            });
            $(slider).slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 2,
                variableWidth: true,
                prevArrow: $('.catIn-prev'),
                nextArrow: $('.catIn-next'),
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            //variableWidth: false,
                        }
                    }
                ]
            });
        }
    }
    if (slider) {
        document.addEventListener('DOMContentLoaded', function () {
            init(slider);
            /!*if (window.innerWidth < 990) {
                init(slider);
            }*!/
        });
        /!*  window.addEventListener('resize', function () {
              if (window.innerWidth < 990) {
                  init(slider);
              } else {
                  window.slider = false;
                  if (slider.classList.contains('slick-initialized')) {
                      $(slider).slick('unslick');
                  }
              }
          });*!/
    }
}

catalogSlider();
*/

$(function () {
  $(".accordionChoice__item .accordionChoice__item-title").on("click", function (e) {
    e.preventDefault();
    var $this = $(this);

    if (!$this.hasClass("active")) {
      $(".accordionChoice__content").slideUp(400);
      $(".accordionChoice__item .accordionChoice__item-title").removeClass("active");
    }

    $this.toggleClass("active");
    $this.next().slideToggle();
  });
}); //$('#custom-select').ddslick({});

$(document).ready(function () {
  $(function () {
    $(".accordionInner__item .accordionInner__title").on("click", function (e) {
      e.preventDefault();
      var $this = $(this);

      if (!$this.hasClass("active")) {
        $(".accordionInner__content").slideUp(400);
        $(".accordionInner__item .accordionInner__title").removeClass("active");
      }

      $this.toggleClass("active");
      $this.next().slideToggle();
    });
  });
});
/*Dropdown Menu*/

var selectItem = function selectItem() {
  var substrate = document.querySelector('.substrate');
  $('.dropdown').click(function (e) {
    if (e.target === $(this)[0] && !e.target.classList.contains('active')) {
      $(this).attr('tabindex', 1).focus();
      $(this).toggleClass('active');
      $(this).find('.dropdown-menu').slideToggle(300);

      if (window.innerWidth < 450) {
        substrate.classList.add('active');
      }
    }
  });
  $('.dropdown').on('change', function () {
    $(this).find('.dropdown-menu').slideUp(300);
    $(this).removeClass('active');

    if (window.innerWidth < 450) {
      substrate.classList.remove('active');
    }
  });
  $('.dropdown').focusout(function () {
    $(this).removeClass('active');
    $(this).find('.dropdown-menu').slideUp(300);
  });
  $('.dropdown .dropdown-menu li').click(function () {
    $(this).parents('.dropdown').find('span').text($(this).text());
  });
};

selectItem();
/*End Dropdown Menu*/

var catalogItems = document.querySelector('.catalogItems'),
    line = document.querySelector('.line'),
    lineCard = document.querySelector('.lineCard'),
    ch = document.querySelector('.ch');

if (line) {
  line.addEventListener('click', function () {
    catalogItems.classList.add('lineCard');
    line.classList.add('active');
    ch.classList.remove('active');
  });
}

if (ch) {
  ch.addEventListener('click', function () {
    catalogItems.classList.remove('lineCard');
    line.classList.remove('active');
    ch.classList.add('active');
  });
}

var filterProdBtn = document.querySelector(".filter-prod__btn"),
    filterProd = document.querySelector('.filter-prod'),
    filterProdResult = document.querySelector('.filter-prod__result'),
    filterProdCloseBtn = document.querySelector('.filter-prod__close-btn'),
    substrateBg = document.querySelector('.substrate-bg'),
    header = document.querySelector('.header');

if (filterProdBtn) {
  filterProdBtn.addEventListener('click', function () {
    filterProd.classList.add('active');
    filterProdResult.style.display = 'none';
    substrateBg.classList.add('active');
    header.style.zIndex = '800';
    disableScroll();
  });
}

if (filterProdCloseBtn) {
  filterProdCloseBtn.addEventListener('click', function () {
    filterProd.classList.remove('active');
    filterProdResult.style.display = 'block';
    substrateBg.classList.remove('active');
    header.style.zIndex = '902';
    enableScroll();
  });
}

$(document).ready(function () {
  $('.tel').inputmask("+7 (999)-999-99-99", {
    "placeholder": "_"
  });
  var anchors = document.querySelectorAll('a.scroll-link');
  Array.prototype.forEach.call(anchors, function (anchor) {
    anchor.addEventListener('click', function (e) {
      e.preventDefault();
      var blockID = anchor.getAttribute('href').substr(1);
      $(".js-nav").removeClass("active");
      $(".js-burger").removeClass("active");
      $("body").removeClass("overflow");
      document.getElementById(blockID).scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      });
    });
  });
});

var populatedFalse = function populatedFalse() {
  var input = document.querySelector('#main-form #name');
  var input2 = document.querySelector('#main-form #tel');
  var input3 = document.querySelector('#main-form #email');
  input.classList.remove('populated');
  input2.classList.remove('populated');
  input3.classList.remove('populated');
}; // рекапча


function onCallbackSubmit(token) {
  var form = document.querySelector('#main-form');
  $(form).parsley().on('form:success', function () {
    form.classList.add('success');
    setTimeout(function () {
      form.reset();
      setFilled();
      populatedFalse();
      form.classList.remove('success');
    }, 4000);
  });
  $(form).parsley().validate(); // всегда внизу
}

function onCallbackSubmitRew(token) {
  var form = document.querySelector('#reviews-form');
  $(form).parsley().on('form:success', function () {
    form.classList.add('success');
    setTimeout(function () {
      form.reset();
      setFilled();
      form.classList.remove('success');
    }, 4000);
  });
  $(form).parsley().validate(); // всегда внизу
}

function callbackClose() {
  var form = document.querySelector('#main-form');
  form.reset();
  setFilled();
  form.classList.remove('success');
}

function callbackCloseRew() {
  var form = document.querySelector('#reviews-form');
  form.reset();
  setFilled();
  form.classList.remove('success');
}

var closeFancy = function closeFancy() {
  $.fancybox.close();
};
/*
function doMenuSearch(input) {
    let search = document.querySelector('.menu-detail__wrap-search').clientHeight,
        wrap = document.querySelector('.menu-detail__wrap'),
        results = document.querySelector('.search-results'),
        searchClose = document.querySelector(".search-holder__close");
    if(input.value.length) {
        results.style.height = wrap.clientHeight - parseInt(getComputedStyle(wrap,null).getPropertyValue('padding-Top')) - parseInt(getComputedStyle(wrap,null).getPropertyValue('padding-Bottom')) - search + 'px';
        results.style.display = 'block';
    }
    searchClose.addEventListener('click', () => {
        input.value = '';
        results.style.display = 'none';
    });
}*/
// для поднятия label


function setFilled() {
  var text = document.querySelectorAll('.iText');
  Array.prototype.forEach.call(text, function (el) {
    if (el.classList.contains('pmask')) {
      el.parentNode.classList.add('filled');
    }

    if (el.value.length > 0) {
      el.parentNode.classList.add('filled');
      el.parentNode.classList.add('lbhidd');
    } else {
      el.parentNode.classList.remove('filled');
    }

    el.addEventListener('focus', function () {
      el.parentNode.classList.add('filled');
      el.parentNode.classList.remove('lbhidd');
    });
    el.addEventListener('blur', function () {
      if (el.value.length === 0) {
        el.parentNode.classList.remove('filled');
      } else {
        el.parentNode.classList.add('lbhidd');
      }
    });
  });
}

setFilled();
/*const btn = document.querySelectorAll('.btn');
if (btn) {
    btn.forEach((e) => {
        e.addEventListener('click', (btn) => {
            e.classList.add('active');
            setTimeout(() => {
                e.classList.remove('active');
            }, 100);
        })
    })
}*/

$(document).ready(function () {
  window.Parsley.on('field:error', function (e) {
    this.$element[0].parentNode.classList.add('error');
  });
  window.Parsley.on('field:success', function (e) {
    this.$element[0].parentNode.classList.remove('error');
  });
});
$(function () {
  $('input').on('change', function () {
    var input = $(this);

    if (input.val().length) {
      input.addClass('populated');
    } else {
      input.removeClass('populated');
    }
  });
  $('#form_textarea').on('change', function () {
    var input = $(this);

    if (input.val().length) {
      input.addClass('populated');
    } else {
      input.removeClass('populated');
    }
  });
  /* setTimeout(function() {
       $('#name').trigger('focus');
   }, 500);*/
});

function initMainSlider() {
  var slider = document.querySelector('.intro-wrap');

  if (slider) {
    $(slider).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      var current = slick.$slides.get(currentSlide),
          next = slick.$slides.get(nextSlide);
      console.log(current);

      if (currentSlide < nextSlide) {
        current.classList.add('next');
      } else {
        console.log('0');
      }
    });
    $(slider).on('afterChange', function (event, slick, currentSlide, nextSlide) {
      var current = slick.$slides.get(currentSlide),
          prev = currentSlide > 0 ? slick.$slides.get(currentSlide - 1) : slick.$slides.get(0),
          next = slick.$slides.get(nextSlide);
      prev.classList.remove('next');
    });
    $(slider).slick({
      infinite: true,
      slidesToShow: 1,
      dots: true,
      speed: 500,
      rows: 0,
      verticalSwiping: true,
      //variableWidth: true,
      prevArrow: $('.intro-prev'),
      nextArrow: $('.intro-next'),
      dotsClass: 'main-dots',
      vertical: true
    });
  }
}

document.addEventListener('DOMContentLoaded', function () {
  initMainSlider();
});
/*
function leaderSlider() {
    let slider = document.querySelector('.leader');
    window.slider = false;

    function init(slider) {
        function paginLea(slick, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            pagin.dataset.width = 100 / total;
            pagin.style.width = 100 / total + '%';
        }

        function changePaginLea(slick, current, next, direction, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            next = next + 1;
            let paginW = 100 / total * next;
            pagin.dataset.width = paginW;
            pagin.style.width = paginW + '%';
        }

        if (slider && !window.slider) {
            window.slider = true;
            $(slider).on('init', function (event, slick, currentSlide, nextSlide) {
                paginLea(slick, 5);
            })
            $(slider).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                if (nextSlide > currentSlide) {
                    changePaginLea(slick, currentSlide, nextSlide, true, 5);
                } else {
                    changePaginLea(slick, currentSlide, nextSlide, false, 5);
                }
            });
            $(slider).slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                //prevArrow: $('.recommend-prev'),
                //nextArrow: $('.recommend-next'),
                //variableWidth: true,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            //variableWidth: false,
                        }
                    }
                ]
            });
        }
    }
    if (slider) {
        document.addEventListener('DOMContentLoaded', function () {
            init(slider);
            if (window.innerWidth < 768) {
                init(slider);
            }
        });
          window.addEventListener('resize', function () {
              if (window.innerWidth < 768) {
                  init(slider);
              } else {
                  window.slider = false;
                  if (slider.classList.contains('slick-initialized')) {
                      $(slider).slick('unslick');
                  }
              }
          });
    }
}

leaderSlider();
*/

/*
function newSlider() {
    let slider = document.querySelector('.newItem-slider');
    window.slider = false;

    function init(slider) {
        function paginNew(slick, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            pagin.dataset.width = 100 / total;
            pagin.style.width = 100 / total + '%';
        }

        function changePaginNew(slick, current, next, direction, index) {
            let pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
                total = slick.slideCount;
            next = next + 1;
            let paginW = 100 / total * next;
            pagin.dataset.width = paginW;
            pagin.style.width = paginW + '%';
        }

        if (slider && !window.slider) {
            window.slider = true;
            $(slider).on('init', function (event, slick, currentSlide, nextSlide) {
                paginNew(slick, 6);
            })
            $(slider).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                if (nextSlide > currentSlide) {
                    changePaginNew(slick, currentSlide, nextSlide, true, 6);
                } else {
                    changePaginNew(slick, currentSlide, nextSlide, false, 6);
                }
            });
            $(slider).slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                //prevArrow: $('.recommend-prev'),
                //nextArrow: $('.recommend-next'),
                //variableWidth: true,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            //variableWidth: false,
                        }
                    }
                ]
            });
        }
    }
    if (slider) {
        document.addEventListener('DOMContentLoaded', function () {
            init(slider);
            if (window.innerWidth < 768) {
                init(slider);
            }
        });
        window.addEventListener('resize', function () {
            if (window.innerWidth < 768) {
                init(slider);
            } else {
                window.slider = false;
                if (slider.classList.contains('slick-initialized')) {
                    $(slider).slick('unslick');
                }
            }
        });
    }
}

newSlider();
*/

function intoSlider() {
  var slider = document.querySelector('.recommend');
  window.slider = false;

  function init(slider) {
    function pagin(slick, index) {
      var pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
          total = slick.slideCount;
      pagin.dataset.width = 100 / total;
      pagin.style.width = 100 / total + '%';
    }

    function changePagin(slick, current, next, direction, index) {
      var pagin = document.querySelector('.pagin[data-pagin="' + index + '"] div'),
          total = slick.slideCount;
      next = next + 1;
      var paginW = 100 / total * next;
      pagin.dataset.width = paginW;
      pagin.style.width = paginW + '%';
    }

    if (slider && !window.slider) {
      window.slider = true;
      $(slider).on('init', function (event, slick, currentSlide, nextSlide) {
        pagin(slick, 3);
      });
      $(slider).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        if (nextSlide > currentSlide) {
          changePagin(slick, currentSlide, nextSlide, true, 3);
        } else {
          changePagin(slick, currentSlide, nextSlide, false, 3);
        }
      });
      $(slider).slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: $('.recommend-prev'),
        nextArrow: $('.recommend-next'),
        //variableWidth: true,
        responsive: [{
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1 //variableWidth: false,

          }
        }]
      });
    }
  }

  if (slider) {
    document.addEventListener('DOMContentLoaded', function () {
      init(slider);
      /*if (window.innerWidth < 990) {
          init(slider);
      }*/
    });
    /*  window.addEventListener('resize', function () {
          if (window.innerWidth < 990) {
              init(slider);
          } else {
              window.slider = false;
              if (slider.classList.contains('slick-initialized')) {
                  $(slider).slick('unslick');
              }
          }
      });*/
  }
}

intoSlider();
$('.stock-slider').slick({
  infinite: true,
  slidesToShow: 1,
  dots: true,
  arrows: false,
  dotsClass: 'main2-dots',
  autoplay: true
});

var autoHei = function autoHei() {
  window.addEventListener('resize', function () {
    if (window.innerWidth > 767) {
      $('.stock').each(function () {
        var thc = $(this),
            thcHeight = thc.find('.card').outerHeight();
        thc.find('.bg-i').css('max-height', thcHeight);
      });
    }
  });
};

autoHei();
$(document).ready(function () {
  //burger
  if (document.querySelector(".js-burger")) {
    $(".js-burger").click(function () {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(".header-top").slideUp("active");
        $(".logo").removeClass("active");
        $(".phone").removeClass("active");
        $(".header-main").removeClass("active");
        $(".substrate").removeClass("active");
        $(".search-form").removeClass("active");
        $(".nav").removeClass("active");
        $(".header .container").removeClass("active");
        $(".header-info__icons").removeClass("active");
        $(".city-selectionImg").removeClass("active");
        $(".header").removeClass("active");
        enableScroll();
      } else {
        $(this).addClass("active");
        $(".header-top").slideDown("active");
        $(".logo").addClass("active");
        $(".phone").addClass("active");
        $(".header-main").addClass("active");
        $(".substrate").addClass("active");
        $(".search-form").addClass("active");
        $(".nav").addClass("active");
        $(".header .container").addClass("active");
        $(".header-info__icons").addClass("active");
        $(".city-selectionImg").addClass("active");
        $(".header").addClass("active");
        disableScroll();
      }
    });
  }
});
/*const jsBurger = document.querySelector('.js-burger'),
    headerTop = document.querySelector('.header-top');
jsBurger.addEventListener('click', () => {
    headerTop.style.display = 'flex';
})*/

/*const onCallbackModal = () => {
    const mask = document.querySelector('.mask'),
        jsModal = document.querySelector('#js-modal'),
        modalBtn = document.querySelector('.modal__btn'),
        formThanksClose = document.querySelectorAll('.formClose__btn'),
        formTthanksBtn = document.querySelectorAll('.form-thanks__btn'),
        formModal = document.querySelector('#modal-form');

    mask.style.display = 'block';
    jsModal.style.display = 'block';

    const closeWin = () => {
        closeModal();
        formModal.reset();
        formModal.classList.remove('success');
        setFilled();
    }

    modalBtn.addEventListener('click', () => {
        closeModal();
    })
    formTthanksBtn.forEach((e) => {
        e.addEventListener('click', () => {
            closeWin();
        })
    })
    formThanksClose.forEach((e) => {
        e.addEventListener('click', () => {
            closeWin();
        })
    })

    const closeModal = () => {
        mask.style.display = 'none';
        jsModal.style.display = 'none';
    }
    mask.addEventListener('click', (event) => {
        const target = event.target;

        if (target.closest('.mask')) {
            closeWin();
        }
    })
}*/

/*
document.addEventListener('DOMContentLoaded', () => {
    $('.main-slider').on('afterChange', (a, b, c) => {
        console.log($('.thumb-slider .slick-slide[data-slick-index="' + c + '"]'));
        $('.thumb-slider .slick-slide').removeClass('slick-current');
        $('.thumb-slider .slick-slide[data-slick-index="' + c + '"]').addClass('slick-current');
    })
    $('.main-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        infinite: true,
    });

    $('.thumb-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        infinite: true,
        asNavFor: '.main-slider',
        adaptiveHeight: true,
        vertical: true,
        focusOnSelect: false,
        variableHeight: true,
        variableWidth: false,
        rows: 0,
        centerMode: false,
        centerPadding: 0,
        responsive: [
            {
                breakpoint: 1056,
                settings: {
                    vertical: false,
                }
            },
            {
                breakpoint: 672,
                settings: {
                    vertical: true,
                }
            },
        ]
    });
    $('.thumb-slider .slick-slide').on('click', function (event) {
        $('.thumb-slider .slick-slide').removeClass('slick-current');
        $(this).addClass('slick-current');
        $('.main-slider').slick('slickGoTo', $(this).data('slickIndex'));
    });
});
*/